# Polars

Polars is a blazingly fast DataFrames library implemented in Rust using Apache Arrow Columnar Format as the memory model.  

* Lazy | eager execution
* Multi-threaded
* SIMD
* Query optimization
* Powerful expression API
* Hybrid Streaming (larger than RAM datasets)
* Rust | Python | NodeJS | ...

# Installing
To install polars you can add these lines on Cargo.toml

```yaml
polars = { version = "0.25.1", features = ["lazy"] }
reqwest =  { version = "0.11.12", features = ["blocking"] }
color-eyre = "0.6"
```

## Jupyter Notebook
**<WIP\>**  

Using Docker: 
```bash
docker build -t jupyter-rust .
docker-compose up
```
PS.: I have some problems creating docker image on my computer. 

## User Guide

Polars there is a good documentation guide to start using this tool. 
* https://pola-rs.github.io/polars-book/user-guide/index.html

Below you can check other nice links of polars: 
* [Chevdor Repo](https://github.com/chevdor/docker-evcxr)
* [Polars User Guide](https://pola-rs.github.io/polars-book/user-guide/notebooks/introduction_polars-rs.html)
* [Polars Rust](https://docs.rs/polars/latest/polars/)
* [Polars Doc Lazy](https://docs.rs/polars/latest/polars/docs/lazy/index.html)
* [Polars Eager Api](https://docs.rs/polars/latest/polars/docs/eager/index.html)
* [Hands on Polars in Rust](https://www.legendu.net/misc/blog/hands-on-polars-in-rust/)


## Examples

You can check some code examples in `polars-overview/src/examples.rs` file.
In the main code there are pipeline example used in this article: https://miyake-akio.medium.com/big-data-with-rust-part-2-3-yes-we-can-1fe6cfb01791
## References
* https://github.com/evcxr/evcxr/blob/main/evcxr_jupyter/README.md
* https://jupyter-docker-stacks.readthedocs.io/en/latest/index.html
* https://github.com/jupyter/docker-stacks/tree/main/base-notebook
* https://gitlab.com/nicolalandro/rust-jupyter-notebook
* https://datacrayon.com/posts/programming/rust-notebooks/setup-anaconda-jupyter-and-rust/
* https://rust-lang-nursery.github.io/rust-cookbook/intro.html
