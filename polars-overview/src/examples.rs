use polars::prelude::*;
use polars::df;

fn main() -> PolarsResult<()> {
    let df = df! [
        "crew_member" => ["Luffy","Zoro","Nami","Usopp","Sanji","Chopper","Nico Robin","Franky", "Brook", "Jinbei"],
        "rewards" => [3000000000.00,1111000000.00,360000000.00,500000000.00,1032000000.00,1000.00,939000000.00,400000000.00,380000000.00,1110000000.00],
        "age" => [19,21,20,19,21,17,30,36,90,46],
        "is_punched_tenryuubito" => [true, false,false,false,false,false,false,false,false,false]
    ]?;

    let selected = df.clone().lazy().select([
        col("crew_member"),
        col("rewards")
    ]).collect()?;

    println!("{:?}", selected);

    let filtered = df.lazy()
    .filter(col("rewards").gt(lit(1000000000.0)))
    .collect()?;

    println!("{:?}", filtered);

    let df2 = df! [
        "crew_member" => ["Luffy","Luffy","Zoro","Nami","Usopp","Sanji","Chopper","Nico Robin","Franky", "Brook", "Jinbei"],
        "rewards" => [3000000000.00,30000000.00,1111000000.00,360000000.00,500000000.00,1032000000.00,1000.00,939000000.00,400000000.00,380000000.00,1110000000.00],
        "age" => [19,17,21,20,19,21,17,30,36,90,46],
        "is_punched_tenryuubito" => [true,false,false,false,false,false,false,false,false,false,false]
    ]?;

    let aggregated = df2.lazy()
    .groupby([col("crew_member")])
    .agg([col("rewards").n_unique().alias("unique_rewards"), col("age").max()]).collect()?;

    println!("{:?}", aggregated);

    let df_a = df![
        "crew_member" => ["Luffy","Zoro","Nami"],
        "rewards" => [3000000000.00,1111000000.00,360000000.00],
        "age" => [19,21,20]
    ]?;
    
    let df_b = df![
        "friend" => ["Sanji","Chopper","Nico Robin", "Jinbei"],
        "rewards" => [1032000000.00,1000.00,939000000.00,1110000000.00],
        "race" => ["human", "tanuki", "human","triton"]
        ]?;
    
    let lf_a = df_a.clone().lazy();
    let lf_b = df_b.clone().lazy();
    
    let joined = lf_a.join(lf_b, vec![col("crew_member")], vec![col("friend")], JoinType::Outer).collect()?;
    println!("{:?}", joined);

    joined.schema();
    println!("{}", joined.head(Some(15)));
    println!("{}", joined.height());
    println!("{}", joined.width());

    // Read CSV as LazyFrame 
    let lf:LazyFrame  = LazyCsvReader::new(source_age_data_path)
    .has_header(true)
    .finish()?;

    // Read CSV as DataFrame
    let mut df = CsvReader::from_path(source_age_data_path)?
        .infer_schema(None)
        .has_header(true)
        .finish()?;

    // Write DataFrame as Parquet
    let file = File::create("../data/minilake_polars/raw/output.parquet").expect("could not create file");
    let bfw = BufWriter::new(file);
    let pw = ParquetWriter::new(bfw).with_compression(ParquetCompression::Snappy);
    pw.finish(&mut df)?;
    // Write LazyFrame as Parquet
    let file = File::create("../data/minilake_polars/raw/output.parquet").expect("could not create file");
    let bfw = BufWriter::new(file);
    let pw = ParquetWriter::new(bfw).with_compression(ParquetCompression::Snappy);
    pw.finish(&mut lf.clone().collect()?)?;

    // Tentative of Iteration
    // let dataframes = vec![fato_census_df, 
    // dim_age_census_df,
    // dim_area_census_df,
    // dim_ethnic_census_df,
    // dim_sex_census_df,
    // dim_year_census_df];

    // let raw_paths: Vec<String> = vec![raw_fato_data_path, 
    // raw_age_data_path,
    // raw_area_data_path,
    // raw_ethnic_data_path,
    // raw_sex_data_path,
    // raw_year_data_path
    // ];

    // for (dataframe, raw_path) in dataframes.into_iter().zip(raw_paths.iter()) {
    //     process_raw_data(dataframe, &raw_path)?;
    // }

Ok(())
}

