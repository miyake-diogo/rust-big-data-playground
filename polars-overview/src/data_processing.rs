pub mod data_utils {
    use log::{info, debug};
    use std::path::Path;
    use std::fs;
    use std::io::BufWriter;
    use std::fs::File;
    use polars::prelude::*;

    pub fn save_dataframe_as_parquet(mut dataframe: DataFrame, path: &str) -> PolarsResult<()> {
      println!("\nTry to starting save Dataframe as parquet: {:?}", path);
      debug!("Starting debug");
      let b: bool = Path::new(path).is_dir();
      if b == true {
        fs::remove_dir_all(path)?;
      }

      let file = File::create(path).expect("could not create file");
      let bfw = BufWriter::new(file);
      let pw = ParquetWriter::new(bfw).with_compression(ParquetCompression::Snappy);
      pw.finish(&mut dataframe)?;

      println!("\nDataframe is saved in this folder: {:?}", path);
      Ok(())
    }


    pub async fn process_stage_data(){


    }

    pub async fn save_data_as_parquet(){

    }

}