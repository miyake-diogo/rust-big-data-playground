#[warn(unused_imports)]
use polars::prelude::*;
use crate::data_processing::data_utils::*;
pub mod data_processing;
use std::time::{Instant};
use log::info;
// use std::io::BufWriter;
// use std::fs::File;

fn main() -> PolarsResult<()> {
    // concat Strings
    // let together = owned_string.clone() + borrowed_string;
    /* ======================================
        Source Path 
       ====================================== 
    */
    let base_path : String = "/Users/diogomiyake/projects/rust/rust-big-data-playground/".to_owned();
    let source_age_data_path = base_path.clone() + "/data/source/age_census/DimenLookupAge8277.csv";
    let source_area_data_path = base_path.clone() + "/data/source/area_census/DimenLookupArea8277.csv";
    let source_ethnic_data_path = base_path.clone() + "/data/source/ethnic_census/DimenLookupEthnic8277.csv";
    let source_sex_data_path = base_path.clone() + "/data/source/sex_census/DimenLookupSex8277.csv";
    let source_year_data_path = base_path.clone() + "/data/source/year_census/DimenLookupYear8277.csv";
    let source_fato_data_path = base_path.clone() + "/data/source/fato_census/Data8277.csv";
    
    /* ======================================
        Path for raw 
       ====================================== 
    */    
    let raw_age_data_path = "../data/minilake_polars/raw/census/raw_age_census.parquet";
    let raw_area_data_path = "../data/minilake_polars/raw/census/raw_area_census.parquet";
    let raw_ethnic_data_path = "../data/minilake_polars/raw/census/raw_ethnic_census.parquet";
    let raw_sex_data_path = "../data/minilake_polars/raw/census/raw_sex_census.parquet";
    let raw_year_data_path = "../data/minilake_polars/raw/census/raw_year_census.parquet";
    let raw_fato_data_path = "../data/minilake_polars/raw/census/raw_fato_census.parquet";    
    /* ======================================
        Path for stage 
       ====================================== 
    */
    let stage_age_data_path = "../data/minilake_polars/stage/census/stage_age_census.parquet";
    let stage_area_data_path = "../data/minilake_polars/stage/census/stage_area_census.parquet";
    let stage_ethnic_data_path = "../data/minilake_polars/stage/census/stage_ethnic_census.parquet";
    let stage_sex_data_path = "../data/minilake_polars/stage/census/stage_sex_census.parquet";
    let stage_year_data_path = "../data/minilake_polars/stage/census/stage_year_census.parquet";
    let stage_fato_data_path = "../data/minilake_polars/stage/census/stage_fato_census.parquet";
    /* ======================================
        Path for Curated 
       ====================================== 
    */
    let curated_obt_data_path = "../data/minilake_polars/curated/obt_census/curated_obt_census.parquet";
    let agg_fato_data_path = "../data/minilake_polars/curated/obt_census/agg_obt_census.parquet";
    /* ======================================
    Reading Data
    ====================================== 
    */

    let dim_age_census_df = CsvReader::from_path(source_age_data_path)?
    .infer_schema(None)
    .has_header(true)
    .finish()?;

    let dim_area_census_df = CsvReader::from_path(source_area_data_path)?
    .infer_schema(None)
    .has_header(true)
    .finish()?;

    let dim_ethnic_census_df = CsvReader::from_path(source_ethnic_data_path)?
    .infer_schema(None)
    .has_header(true)
    .finish()?;

    let dim_sex_census_df = CsvReader::from_path(source_sex_data_path)?
    .infer_schema(None)
    .has_header(true)
    .finish()?;

    let dim_year_census_df = CsvReader::from_path(source_year_data_path)?
    .infer_schema(None)
    .has_header(true)
    .finish()?;

    // Add Schema to Fato DataFrame
    let schema = Schema::from(vec![
        Field::new("Year", DataType::Int64),
        Field::new("Age", DataType::Int64),
        Field::new("Ethnic", DataType::Int64),
        Field::new("Sex", DataType::Int64),
        Field::new("Area", DataType::Utf8),
        Field::new("count", DataType::Utf8),
    ].into_iter());

    let fato_census_df = CsvReader::from_path(source_fato_data_path)?
    .with_schema(&schema)
    .has_header(true)
    .finish()?;

    /* ======================================
    Processing raw Data 
    ====================================== 
    */

    // // Check Time od process data
    let start = Instant::now();
    save_dataframe_as_parquet(dim_age_census_df.clone(), &raw_age_data_path)?;
    save_dataframe_as_parquet(dim_area_census_df.clone(), &raw_area_data_path)?;
    save_dataframe_as_parquet(dim_ethnic_census_df.clone(), &raw_ethnic_data_path)?;
    save_dataframe_as_parquet(dim_sex_census_df.clone(), &raw_sex_data_path)?;
    save_dataframe_as_parquet(dim_year_census_df.clone(), &raw_year_data_path)?;
    save_dataframe_as_parquet(fato_census_df.clone(), &raw_fato_data_path)?;
    let duration = start.elapsed();
    println!("\nTime elapsed in all of save_dataframe_as_parquet function is: {:?}", duration);
    

    /* ======================================
    Processing Stage Data 
    ====================================== 
    */
    info!("Starting Transforming Data to Stage Step");
    let stg_start = Instant::now();
    //Cast column datatype
    let fato_census_df = fato_census_df.clone().lazy().select(
        [col("Year"),
        col("Age"),
        col("Ethnic"),
        col("Sex"),
        col("Area"),
        col("count"),
        when(col("count") == lit("..C")).then(0).otherwise(col("count")).cast(DataType::Float64).alias("total_count")
        ]).collect()?;

    save_dataframe_as_parquet(dim_age_census_df.clone(), &stage_age_data_path)?;
    save_dataframe_as_parquet(dim_area_census_df.clone(), &stage_area_data_path)?;
    save_dataframe_as_parquet(dim_ethnic_census_df.clone(), &stage_ethnic_data_path)?;
    save_dataframe_as_parquet(dim_sex_census_df.clone(), &stage_sex_data_path)?;
    save_dataframe_as_parquet(dim_year_census_df.clone(), &stage_year_data_path)?;
    save_dataframe_as_parquet(fato_census_df.clone(), &stage_fato_data_path)?;

    let stg_duration = stg_start.elapsed();
    println!("\nTime elapsed to save Data in Stage is: {:?}", stg_duration);

    /* ======================================
    Processing Curated Data 
    ====================================== 
    */

    // Just making a join with aggregation  and save data in curated layer
    let lf_a = fato_census_df.clone().lazy();
    let lf_b = dim_age_census_df.clone().lazy();
    let partial_joined = lf_a.join(lf_b, vec![col("Age")], vec![col("Code")], JoinType::Inner).collect()?;
    // println!("{:?}", partial_joined.head(Some(5)));

    // Aggregating Data
    let aggregated = partial_joined.clone().lazy()
    .groupby([col("Year")])
    .agg([
        col("total_count").sum().alias("sum_total_count"), 
        col("total_count").max().alias("max_total_count")
    ]).collect()?;

    // println!("{:?}", aggregated.head(Some(5)));

    save_dataframe_as_parquet(partial_joined, &curated_obt_data_path)?;
    save_dataframe_as_parquet(aggregated.clone(), &agg_fato_data_path)?;

    Ok(())
}
