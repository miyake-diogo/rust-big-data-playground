# Intro Fluvio

# Installing
Reference: https://www.fluvio.io/docs/get-started/mac/

``` bash
curl -fsS https://packages.fluvio.io/v1/install.sh | bash

```
**Using Fluvio Cloud:**  
https://www.fluvio.io/docs/get-started/cloud/

**Cloud Setup:** 
https://www.fluvio.io/docs/tutorials/cloud-setup/

## First Steps:
Ref.: https://www.fluvio.io/docs/tutorials/cloud-setup/  

**Create topics**  
`fluvio topic create greetings`  

**Produce**  
`echo 'Hello world!' | fluvio produce greetings`  
Run `fluvio produce greetings` after write any message to produce to topic  

**Consume**  
`fluvio consume greetings -dB`  
Consume from the ending of topic `fluvio consume greetings`  
Consume from the beginning of topic `fluvio consume greetings -dB`  

## Create a data pipeline
Ref.:  https://www.fluvio.io/docs/tutorials/data-pipeline/  

There are two main steps for this tutorial:

* Creating an Inbound HTTP Connector to collect JSON
* Creating an Outbound SQL Connector to insert the input JSON into a database
  * Basic insert
  * JSON to JSON transformation before insert

## Hello World Example
Ref.:  https://www.fluvio.io/api/official/rust/examples/
