use async_std::stream::StreamExt;
use fluvio::metadata::topic::TopicSpec;
use fluvio::{Fluvio, RecordKey};
use rand::Rng;
use serde::{Serialize, Deserialize};

const TOPIC_NAME: &str = "customer";
const PARTITION_NUM: u32 = 0;
const PARTITIONS: u32 = 1;
const REPLICAS: u32 = 1;

#[derive(Serialize, Deserialize, Debug)]
struct Customer {
    id: i32,
    name: String,
    email: String,
}

impl Customer {
    fn random() -> Self {
        let mut rng = rand::thread_rng();
        Customer {
            id: rng.gen_range(1..=1000000),
            name: format!("Customer-{}", rng.gen::<u32>()),
            email: format!("customer-{}@example.com", rng.gen::<u32>()),
        }
    }
}

#[async_std::main]
async fn main() {
    // Connect to Fluvio cluster
    let fluvio = Fluvio::connect().await.unwrap();

    // Create a topic
    let admin = fluvio.admin().await;
    let topic_spec = TopicSpec::new_computed(PARTITIONS, REPLICAS, None);
    let _topic_create = admin
        .create(TOPIC_NAME.to_string(), false, topic_spec)
        .await;

    // Produce to a topic
    let producer = fluvio::producer(TOPIC_NAME).await.unwrap();
    for _ in 0..1000 {
        let customer = Customer::random();
        let record_key = RecordKey::NULL;
        let record_value = serde_json::to_string(&customer).unwrap();
        producer.send(record_key, record_value).await.unwrap();
    }
    producer.flush().await.unwrap();

}
