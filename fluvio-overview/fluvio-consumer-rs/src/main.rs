use async_std::stream::StreamExt;
use fluvio::metadata::topic::TopicSpec;
use fluvio::{Fluvio, RecordKey};
use serde::{Deserialize, Serialize};

const TOPIC_NAME: &str = "customer";
const PARTITION_NUM: u32 = 0;

#[derive(Serialize, Deserialize, Debug)]
struct Customer {
    id: i32,
    name: String,
    email: String,
}

#[async_std::main]
async fn main() {
    // Connect to Fluvio cluster
    let fluvio = Fluvio::connect().await.unwrap();

    // Create a consumer
    let consumer = fluvio::consumer(TOPIC_NAME, PARTITION_NUM).await.unwrap();
    let mut stream = consumer.stream(fluvio::Offset::from_end(10)).await.unwrap();

    // Consume records from the topic
    while let Some(Ok(record)) = stream.next().await {
        let customer: Customer = serde_json::from_slice(record.value()).unwrap();
        println!("{:?}", customer);
    }
}