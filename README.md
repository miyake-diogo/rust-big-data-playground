# Rust Big Data Playground

Repository with examples of Rust frameworks for Big Data

## Install Rust

First you need to install Rust, You can follow steps on this page:  
[Link](https://www.rust-lang.org/tools/install)

## Datafusion
[Datafusion Page](datafusion-overview/Datafusion.md)

## Polars
WIP - Work in progress 

## Fluvio
WIP - Work in progress


## References

[Rust Book](https://doc.rust-lang.org/book/)
[Datafusion Page](https://arrow.apache.org/datafusion/user-guide/introduction.html)
[Dataset Census](stats.govt.nz/large-datasets/csv-files-for-download/)
