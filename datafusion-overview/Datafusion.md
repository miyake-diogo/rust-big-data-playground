# DataFusion Overview

Overview of Datafusion library for use dataframe api to make batch ingestion and transformations in Rust programming language.  
DataFusion is an extensible query execution framework, written in Rust, that uses Apache Arrow as its in-memory format.  

DataFusion supports both an SQL and a DataFrame API for building logical query plans as well as a query optimizer and execution engine capable of parallel execution against partitioned data sources (CSV and Parquet) using threads.  

**Note:** Datafusion have some methods like apache spark, it help to understand and use.  
## To run

1. Download dataset
2. Add datafusion on toml file
3. Go to terminal and use cargo run command
## Dataset

Covid Dataset is located in this [link](https://ourworldindata.org/explorers/coronavirus-data-explorer?facet=none&Metric=Confirmed+deaths&Interval=Cumulative+%28last+12+months%29&Relative+to+Population=true&Color+by+test+positivity=false)

The Statistic Dataset are located in this [link](https://www.stats.govt.nz/information-releases/statistical-area-1-dataset-for-2018-census-updated-march-2020)

First donwload, after it extract and create this folder structure: 
```bash
data
└── minilake
    ├── curated
    │   ├── aggregated_tables
    │   │   ├── part-0.parquet
    │   │   ├── part-1.parquet
    │   │   ├── part-2.parquet
    │   │   ├── part-3.parquet
    │   │   ├── part-4.parquet
    │   │   ├── part-5.parquet
    │   │   ├── part-6.parquet
    │   │   ├── part-7.parquet
    │   │   ├── part-8.parquet
    │   │   └── part-9.parquet
    │   └── final_obt
    │       ├── part-0.parquet
    │       ├── part-1.parquet
    │       ├── part-2.parquet
    │       ├── part-3.parquet
    │       ├── part-4.parquet
    │       ├── part-5.parquet
    │       ├── part-6.parquet
    │       ├── part-7.parquet
    │       ├── part-8.parquet
    │       └── part-9.parquet
    ├── raw
    │   ├── age_census
    │   │   └── DimenLookupAge8277.csv
    │   ├── area_census
    │   │   └── DimenLookupArea8277.csv
    │   ├── covid
    │   │   └── owid-covid-data.csv
    │   ├── ethnic_census
    │   │   └── DimenLookupEthnic8277.csv
    │   ├── fato_census
    │   │   └── Data8277.csv
    │   ├── sex_census
    │   │   └── DimenLookupSex8277.csv
    │   └── year_census
    │       └── DimenLookupYear8277.csv
    └── stage
        ├── age_census
        │   └── part-0.parquet
        ├── area_census
        │   └── part-0.parquet
        ├── covid
        │   └── part-0.parquet
        ├── ethnic_census
        │   └── part-0.parquet
        ├── fato_census
        │   └── part-0.parquet
        ├── sex_census
        │   └── part-0.parquet
        └── year_census
            └── part-0.parquet
```

Note that I rename file names because is so big.The complete step by step is located in reference article that I wroted in medium.  

## Disclaimer 
I Tried to work with this dataset but I had problems described in this [issue](https://github.com/apache/arrow-datafusion/issues/3701) and [question](https://github.com/apache/arrow/issues/14313) after it I decide to move to covid Dataset described above.  
Another issue: [Datafusion Not save all lines of csv](https://github.com/apache/arrow-datafusion/issues/3783)  

Below we have Dataset link with notes:
[link](https://www.stats.govt.nz/information-releases/statistical-area-1-dataset-for-2018-census-updated-march-2020)

Code tested to open issue:  
```rust
use datafusion::prelude::*;
use log::{debug, info, LevelFilter, trace};
use crate::datapipeline::data_utils::*;
pub mod datapipeline;
use datafusion::logical_plan::{when};
// use datafusion::scalar::ScalarValue::Int64;

use datafusion::arrow::datatypes::DataType::{Int64,Utf8};
#[tokio::main]
async fn main() -> datafusion::error::Result<()> {
  let ctx: SessionContext = SessionContext::new();
  let raw_fato_path: &str = "data/minilake/raw/fato_census/Data8277.csv";
  let fato_census_df = ctx.read_csv(raw_fato_path,  
                                  CsvReadOptions::new()).await?;

  let fato_census_df = fato_census_df
                                             .select(vec![
                                              col("Year").alias("year"),
                                              col("Age").alias("age"),
                                              col("Ethnic").alias("ethnic"),
                                              col("Sex").alias("sex") ,
                                              col("Area").alias("area"),
                                              col("count").alias("total_count")
                                             ])?;
  
  // We can see the ..C values in Count column
  fato_census_df.show_limit(20).await?;
  println!("Print schema before change\n");
  print_schema_of_dataframe(&fato_census_df).await?;
  // Create a function to make trnasformation
  let transform_count_data = when(col("total_count").eq(lit("..C")), lit(0_u32)).otherwise(col("total_count"))?;
  //Cast column datatype
  let fato_census_df = fato_census_df.with_column("total_count",cast(transform_count_data, Int64))?;
  fato_census_df.show_limit(25).await?;

  let fato_census_df = fato_census_df.with_column("area",cast(col("area"),Utf8))?;
  println!("Print schema after change\n");
  print_schema_of_dataframe(&fato_census_df).await?;
  fato_census_df.filter(col("area").eq(lit("CMB07602")))?.show_limit(5).await?;
  // let stage_fato_path: &str = "data/minilake/stage/fato_census/";
  // save_data_as_parquet(&fato_census_df, stage_fato_path).await?;
  // // print_schema_of_dataframe(&fato_census_df2).await?;
  // let fato_census_df = ctx.read_parquet("data/minilake/stage/fato_census/part-0.parquet",ParquetReadOptions::default()).await?;                                            
  // fato_census_df.filter(col("area").eq(lit("CMB07602")))?.show_limit(5).await?;


  Ok(())
  }
```
Folder with codes of issues: 
* [Pipeline for statistics dataset](datafusion-overview/src/statistics_dataset/pipeline_statistics_dataset.rs)
* Corrupt parquet file when save [Covid dataset](datafusion-overview/src/covid_dataset/main.rs) 


**Important Info:**  

You may wish to replace some characters with a numerical value to help you process the data. These characters are:  

confidentialised cells, represented by ‘C’  
figure not available, represented by ‘..’.  


I download data manually and extract in folder, I don't make code to automate this because at this moment I focus only on Big Data Frameworks.  

## Intro to library

Add on cargo.toml below dependencies line:  

``` yaml
[dependencies]
datafusion = "11.0"
tokio = "1.0"
```

Run `cargo build` to install libraries.  
**Note:** If you have some problems with parquet or arrow you need to update rustup.  

## First create a context

You can create session context to run your processing queries or methods.
``` rust
    let ctx: SessionContext = SessionContext::new();
``` 

## Read a CSV
To read csv we can use the 2 different methods:

**Dataframe API**
``` rust
    // read csv to dataframe
    let dataframe = ctx.read_csv("file.csv", CsvReadOptions::new()).await?;

    // read parquet
    let dataframe = ctx.read_parquet("path/part-0.parquet",ParquetReadOptions::default()).await?;
``` 

If you want to pass methods in CsvReadOptions you can pass after `.new()`

``` rust
    // read csv read csv to dataframe passing method delimiter to enum CsvReadOptions
    let dataframe = ctx.read_csv("file.csv", CsvReadOptions::new().delimiter(b';')).await?;
    // execute and print results (100 lines)
    dataframe.show_limit(100).await?;
    Ok(())
``` 

**SQL API**
``` rust 
    // Register table from csv
    ctx.register_csv("table_name", "file.csv", CsvReadOptions::new()).await?;
    // Create a plan to run a SQL query
    let dataframe = ctx.sql("SELECT * FROM table_name").await?;
    // execute and print results
    dataframe.show().await?; // show entirely dataframe to show n lines use show_limit
    Ok(())
``` 

## Check the schema for dataframe

An option to print schema
``` rust 
    // Print Schema
    let schema = dataframe.schema();
    println!("Schema: \n{:#?}", schema);
``` 

## Save files

Write in parquet
**Note:** It's important to have folders created before save
``` rust
    // Write dataframe in parquet
    dataframe.write_parquet("folder_to_save/{}",None).await?;
    //partitioned
    dataframe.repartition(Partitioning::Hash(vec![col("id")], 2))?
        .write_parquet("name.parquet", None).await?;
```
## Transformations
Now it's time to make some transformations of data:
We can do some queries and exploratory data analysis in our dataset.  
Examples: 

### Select Columns

``` rust
    let dataframe = dataframe.select(vec![col("a"), col("b")])?;
    dataframe.show_limit(5).await?;
```
### 

### Filter Columns
``` rust
    dataframe.filter(col("column_name").eq(lit(0_i32)))?.show_limit(5).await?;
```
In eq function we need to pass data type, remember Rust is Strongly typed language.

### Distinct
``` rust
  let dataframe = dataframe.distinct()?;
  dataframe.show().await?;
```

### Change Column Type
``` rust 
  let dataframe = dataframe.with_column("column_name",cast(function, Int64))?;
```

### Case When Then
``` rust 
  //Use a auxiliar function
  let function = when(col("column_name").eq(lit("stringtoget")), lit(0_u32)).otherwise(col("column_name"))?;
  let dataframe = dataframe.with_column("column_name",cast(function, Int64))?;
```

### Concat Columns 

``` rust 
  let expr1 = vec![col("column_name1") ,col("column_name2")];
  let dataframe = dataframe.with_column("column_name",concat_ws("-", &expr1))?;
```
### Join

``` rust
    // the lenght need to be the same and columns needed to be diferent names
    let join_dataframe = left_dataframe.join(right_dataframe, JoinType::Inner, &["col_a", "col_b"], &["col_a2", "col_b2"], None)?;
```
### Aggregate

``` rust
    // examples from documentation
    // The following use is the equivalent of "SELECT MIN(b) GROUP BY a"
    let _ = dataframe.aggregate(vec![col("a")], vec![min(col("b"))])?;

    // The following use is the equivalent of "SELECT MIN(b)"
    let _ = dataframe.aggregate(vec![], vec![min(col("b"))])?;

    // another example
    let agg_df = dataframe.aggregate(vec![col("col1"),col("col2"),col("col3"),col("col4"),col("col5")],
     vec![sum(col("total_count"))])?;
```


## References

* [My Datafusion Article: Big Data with RUST Part 1/3 - Yes we can!!!](https://miyake-akio.medium.com/big-data-with-rust-part-1-3-yes-we-can-fd396410e35)
* [Data Used](https://www.stats.govt.nz/large-datasets/csv-files-for-download/) | [Download Data](https://www3.stats.govt.nz/2018census/Age-sex-by-ethnic-group-grouped-total-responses-census-usually-resident-population-counts-2006-2013-2018-Censuses-RC-TA-SA2-DHB.zip?_ga=2.20958136.102282402.1663639578-985979153.1663098055)
* [Datafusion](https://arrow.apache.org/datafusion/user-guide/example-usage.html)
* [Datafusion Documentation](https://docs.rs/datafusion/latest/datafusion/index.html)
* [Dataframe Datafusion Documentation](https://docs.rs/datafusion/latest/datafusion/dataframe/struct.DataFrame.html)
* [Datafusion Article: data-processing-with-rust-and-apache-arrow-datafusion-introduction](https://medium.com/@MatthieuL49/data-processing-with-rust-and-apache-arrow-datafusion-introduction-d036cc96a3f4)
* [Datafusion Article: writing-a-data-pipeline-in-rust-with-datafusion](https://medium.com/towardsdev/writing-a-data-pipeline-in-rust-with-datafusion-25b5e45410ca)
* https://docs.rs/datafusion/latest/datafusion/prelude/enum.Expr.html
* https://docs.rs/datafusion/latest/datafusion/dataframe/struct.DataFrame.html
* [Youtube Videos: Andy Grove- Ballista: Distributed Compute with Rust and Apache Arrow](https://www.youtube.com/watch?v=ZZHQaOap9pQ)
* [Youtube Videos: DataFusion and Arrow: Supercharge Your Data Analytical Tool with a Rusty Query Engine](https://www.youtube.com/watch?v=Rii1VTn3seQ)
* [Youtube Videos: Datafusion: embeddable query engine written in Rust - Boaz Berman](https://www.youtube.com/watch?v=5q_KwWhCRak)