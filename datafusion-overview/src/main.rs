use datafusion::prelude::*;
use log::info;
use crate::datapipeline::data_utils::*;
pub mod datapipeline;
use datafusion::arrow::datatypes::DataType::{Float32};
use datafusion::arrow::datatypes::{DataType,Field,Schema};
use std::time::{Instant};
#[tokio::main]
async fn main() -> datafusion::error::Result<()> {
  let ctx: SessionContext = SessionContext::new();
  /* ======================================
  Path for source 
  ====================================== 
  */
  let source_age_path: &str = "../data/source/age_census/DimenLookupAge8277.csv";
  let source_area_path: &str = "../data/source/area_census/DimenLookupArea8277.csv";
  let source_ethnic_path: &str = "../data/source/ethnic_census/DimenLookupEthnic8277.csv";
  let source_sex_path: &str = "../data/source/sex_census/DimenLookupSex8277.csv";
  let source_year_path: &str = "../data/source/year_census/DimenLookupYear8277.csv";
  let source_fato_path: &str = "../data/source/fato_census/Data8277.csv";

  /* ======================================
  Path for raw 
  ====================================== 
  */
  let raw_age_path: &str = "../data/minilake_datafusion/raw/age_census/";
  let raw_area_path: &str = "../data/minilake_datafusion/raw/area_census/";
  let raw_ethnic_path: &str = "../data/minilake_datafusion/raw/ethnic_census/";
  let raw_sex_path: &str = "../data/minilake_datafusion/raw/sex_census/";
  let raw_year_path: &str = "../data/minilake_datafusion/raw/year_census/";
  let raw_fato_path: &str = "../data/minilake_datafusion/raw/fato_census/";
  /* ======================================
  Path for stage 
  ====================================== 
  */

  let stage_age_path: &str = "../data/minilake_datafusion/stage/age_census/";
  let stage_area_path: &str = "../data/minilake_datafusion/stage/area_census/";
  let stage_ethnic_path: &str = "../data/minilake_datafusion/stage/ethnic_census/";
  let stage_sex_path: &str = "../data/minilake_datafusion/stage/sex_census/";
  let stage_year_path: &str = "../data/minilake_datafusion/stage/year_census/";
  let stage_fato_path: &str = "../data/minilake_datafusion/stage/fato_census/";

  /* ======================================
  Path for stage 
  ====================================== 
  */
  let partial_obt_path: &str = "../data/minilake_datafusion/curated/final_obt/";
  let aggregated_data_by_sex_path: &str = "../data/minilake_datafusion/curated/aggregated_tables/";

  // /* ======================================
  // Dataframe Creation Stage 
  // ====================================== 
  // */
  
  info!("Starting creating Context and read data");
  let raw_start = Instant::now();
  let schema = Schema::new(vec![
          Field::new("Year", DataType::Int64, false),
          Field::new("Age", DataType::Int64, false),
          Field::new("Ethnic", DataType::Int64, true),
          Field::new("Sex", DataType::Int64, false),
          Field::new("Area", DataType::Utf8, true),
          Field::new("count", DataType::Utf8, false),
      ]);

  let csv_read_option = CsvReadOptions {
        // Update the option provider with the defined schema
        schema: Some(&schema),
        ..Default::default()
    };
  let fato_census_df = ctx.read_csv(source_fato_path,  
                                                csv_read_option).await?;

  let dim_age_census_df = ctx.read_csv( source_age_path, CsvReadOptions::new()).await?;
  // Another form to create a dataframe
  ctx.register_csv("dim_area", source_area_path, CsvReadOptions::new()).await?;
  // Create a plan to run a SQL query
  let dim_area_census_df = ctx.sql("SELECT * FROM dim_area").await?;

  let dim_ethnic_census_df = ctx.read_csv(source_ethnic_path,CsvReadOptions::new()).await?;
  let dim_sex_census_df = ctx.read_csv(source_sex_path,CsvReadOptions::new()).await?;
  let dim_year_census_df = ctx.read_csv(source_year_path,CsvReadOptions::new()).await?;

  /* ======================================
  save Raw 
  ====================================== 
  */
  info!("Saving raw files");
  save_data_as_parquet(dim_age_census_df.clone(), raw_age_path).await?;
  save_data_as_parquet(dim_area_census_df.clone(), raw_area_path).await?;
  save_data_as_parquet(dim_ethnic_census_df.clone(), raw_ethnic_path).await?;
  save_data_as_parquet(dim_sex_census_df.clone(), raw_sex_path).await?;
  save_data_as_parquet(dim_year_census_df.clone(), raw_year_path).await?;
  save_data_as_parquet(fato_census_df.clone(), raw_fato_path).await?;
  let raw_duration = raw_start.elapsed();
  println!("Time elapsed in all of save data in raw is: {:?}", raw_duration);
  /* ======================================
  Transformation Stage - Schema Definition 
  ====================================== 
  */
  info!("Starting Transforming Data to Stage Step");
  let stg_start = Instant::now();

  // let dim_age_census_df = dim_age_census_df
  // .select(vec![col("Code").alias("age_code"),col("Description").alias("age_description")])?;
  // // dim_age_census_df.clone().show_limit(5).await?;

  // let dim_area_census_df = dim_area_census_df
  //   .select(vec![col("Code").alias("area_code"),col("Description").alias("area_description")])?;
  // // dim_area_census_df.clone().show_limit(5).await?;

  // let dim_ethnic_census_df = dim_ethnic_census_df
  //   .select(vec![col("Code").alias("ethnic_code"),col("Description").alias("ethnic_description")])?;
  // // dim_ethnic_census_df.clone().show_limit(5).await?;

  // let dim_sex_census_df = dim_sex_census_df
  //   .select(vec![col("Code").alias("sex_code"), col("Description").alias("sex_description")])?;
  // // dim_sex_census_df.clone().show_limit(5).await?;

  // let dim_year_census_df = dim_year_census_df
  //   .select(vec![col("Code").alias("year_code"),col("Description").alias("year_description")])?;
  // dim_year_census_df.clone().show_limit(5).await?;
  
// let fato_census_df = fato_census_df
//     .select(vec![
//       col("Year").alias("year"),
//       col("Age").alias("age"),
//       col("Ethnic").alias("ethnic"),
//       col("Sex").alias("sex"),
//       col("Area").alias("area"),
//       col("count").alias("total_count")
//       ])?;
  
  // We can see the ..C values in Count column
  // fato_census_df.clone().show_limit(5).await?;
  // print_schema_of_dataframe(fato_census_df.clone()).await?;
  // Create a function to make trnasformation
  let transform_count_data = when(col("count")
    .eq(lit("..C")), lit(0))
    .otherwise(col("count"))?;

  // //Cast column datatype
  let fato_census_df = fato_census_df.with_column(
    "count",
    cast(transform_count_data, Float32))?;
  
    // limiting dataframe becaus ehave problem with parse schema
  // let fato_census_df = fato_census_df.limit(0, Some(10000))?;


  // Save data on silver layer
  info!("Saving Staged files");
  save_data_as_parquet(dim_age_census_df.clone(), stage_age_path).await?;
  save_data_as_parquet(dim_area_census_df.clone(), stage_area_path).await?;
  save_data_as_parquet(dim_ethnic_census_df.clone(), stage_ethnic_path).await?;
  save_data_as_parquet(dim_sex_census_df.clone(), stage_sex_path).await?;
  save_data_as_parquet(dim_year_census_df.clone(), stage_year_path).await?;
  save_data_as_parquet(fato_census_df.clone(), stage_fato_path).await?;
  let stg_duration = stg_start.elapsed();
  println!("Time elapsed in all of save data in stage is: {:?}", stg_duration);

  /* ======================================
  Final Stage - One Big Table Creation
  ====================================== 
  */
  // Make a Join of all data
  let final_start = Instant::now();
  let partial_obt_df = fato_census_df.join(dim_age_census_df, 
                            JoinType::Inner, &["Age"], 
                            &["Code"], 
                             None)?;
  // partial_obt_df.clone().show_limit(5).await?;
  // Create another aggregate tables and save in gold layer


  let agg_df = partial_obt_df.clone().aggregate(
    vec![col("Area"),col("Sex"),col("Ethnic"),col("Year")],
     vec![sum(col("count"))])?;
  // println!("Print agg");  
  // agg_df.clone().show_limit(10).await?;

  println!("Saving Agg DF and OBT DF");
  save_data_as_parquet(partial_obt_df.clone(), partial_obt_path).await?;
  save_data_as_parquet(agg_df.clone(), aggregated_data_by_sex_path).await?;
  
  let final_duration = final_start.elapsed();
  println!("Time elapsed in all of save data in final tables is: {:?}", final_duration);

  Ok(())
  }