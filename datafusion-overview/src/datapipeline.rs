pub mod data_utils {
  use datafusion::prelude::*;
  use log::{info, debug};
  use std::path::Path;
  use std::fs;

    pub async fn save_data_as_parquet( dataframe: DataFrame, path: &str) -> datafusion::error::Result<()> {
      info!("\nTry to starting save Dataframe as parquet: {:?}", path);
      debug!("Starting debug");
      let b: bool = Path::new(path).is_dir();
      if b == true {
        fs::remove_dir_all(path).map_err(|err| println!("{:?}", err)).ok();
      }
      dataframe.clone().write_parquet(path,None).await?;
      info!("\nDataframe is saved in this folder: {:?}", path);
      Ok(())
    }
    
    pub async fn print_schema_of_dataframe(dataframe: DataFrame)-> datafusion::error::Result<()> {
      let df_schema = dataframe.schema();
      println!("\nSchema: \n{:?}", df_schema);
      Ok(())
    }
    
    pub async fn print_n_lines_of_dataframe(dataframe: DataFrame, qty: usize)-> datafusion::error::Result<()> {
      info!("\nPrinting Dataframe {:?} Rows: \n", qty);
      dataframe.clone().show_limit(qty).await?;
      Ok(())
    }

    //TODO: Add read Csv in this function
    // I only add show and printschema to show the format of data are populated
    pub async fn save_and_print_dataframe(dataframe:DataFrame, 
                                  qty: usize, 
                                  path: &str) -> datafusion::error::Result<()> {
          debug!("Starting debug");
          print_n_lines_of_dataframe(dataframe.clone(), qty).await?;
          print_schema_of_dataframe(dataframe.clone()).await?;
          save_data_as_parquet(dataframe.clone(),path).await?;
          info!("Sucess in ingest data on: {:?}", path);
          Ok(())
      }

  }
