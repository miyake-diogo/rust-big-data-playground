use datafusion::prelude::*;


#[tokio::main]
async fn main() -> datafusion::error::Result<()> {
  let ctx: SessionContext = SessionContext::new();
// Example of iteration in DataFame lists
// print first 5 rows of dataframe and your schema
// We can process by loop every function

let dataframes = vec![fato_census_df.clone(), 
dim_age_census_df.clone(),
dim_area_census_df.clone(),
dim_ethnic_census_df.clone(),
dim_sex_census_df.clone(),
dim_year_census_df.clone()];

for dataframe in dataframes.into_iter() {
print_n_lines_of_dataframe(dataframe.clone(), 5).await?;
print_schema_of_dataframe(dataframe.clone()).await?;
}

let fato_census_df = fato_census_df.with_column("area",cast(
  col("Area"),
  Utf8))?;
let fato_census_df = fato_census_df.with_column("Area", regexp_replace(vec![lit("CMB%"), lit("")]))?;


}