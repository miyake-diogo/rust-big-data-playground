use datafusion::prelude::*;
use log::{info, debug};
use crate::datapipeline::data_utils::*;
pub mod datapipeline;


#[tokio::main]
async fn main() -> datafusion::error::Result<()> {
  let ctx: SessionContext = SessionContext::new();
  /* ======================================
  Path for raw 
  ====================================== 
  */
  let raw_covid_path: &str = "data/minilake/raw/covid/owid-covid-data.csv";

  /* ======================================
  Path for stage 
  ====================================== 
  */

  let stage_covid_path: &str = "data/minilake/stage/covid/";

  /* ======================================
  Path for stage 
  ====================================== 
  */
  let curated_covid_path: &str = "data/minilake/curated/covid/";

  // /* ======================================
  // Dataframe Creation Stage 
  // ====================================== 
  // */
  
  info!("Starting creating Context and read data");

  // Create the dataframe
  
  let covid_df = ctx.read_csv(raw_covid_path,  
                                  CsvReadOptions::new()).await?;

  // /* ======================================
  // save Stage 
  // ====================================== 
  // */
  info!("Starting save Stage");
  // print first 5 rows of dataframe and your schema
  // print_n_lines_of_dataframe(&covid_df, 5).await?;
  // print_schema_of_dataframe(&covid_df).await?;

  /* ======================================
  Transformation Stage - Schema Definition 
  ====================================== 
  */
  debug!("Starting debug");
  let covid_df = covid_df.select(vec![
    col("continent"), col("location"), col("date"), col("total_cases"), col("new_cases"), col("total_deaths"),
    col("new_deaths"), col("total_cases_per_million"), col("new_cases_per_million"), col("total_tests"), col("new_tests"), 
    col("diabetes_prevalence"), col("female_smokers"), col("male_smokers"), col("excess_mortality"), col("population")])?;
  // save_data_as_parquet(&covid_df,stage_covid_path).await?;
  covid_df.write_parquet(stage_covid_path,None).await?;

  let covid_df = ctx.read_parquet(stage_covid_path,ParquetReadOptions::default()).await?;
  print_n_lines_of_dataframe(&covid_df, 5).await?;
  
  // covid_df.show_limit(5).await?;

  Ok(())
  }