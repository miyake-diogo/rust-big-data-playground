use datafusion::prelude::*;
use log::{debug, info, LevelFilter, trace};
use crate::datapipeline::data_utils::*;
pub mod datapipeline;
use datafusion::logical_plan::{when};
// use datafusion::scalar::ScalarValue::Int64;

use datafusion::arrow::datatypes::DataType::{Int64,Utf8};
#[tokio::main]
async fn main() -> datafusion::error::Result<()> {
  let ctx: SessionContext = SessionContext::new();
  /* ======================================
  Path for raw 
  ====================================== 
  */
  let raw_age_path: &str = "data/minilake/raw/age_census/DimenLookupAge8277.csv";
  let raw_area_path: &str = "data/minilake/raw/area_census/DimenLookupArea8277.csv";
  let raw_ethnic_path: &str = "data/minilake/raw/ethnic_census/DimenLookupEthnic8277.csv";
  let raw_sex_path: &str = "data/minilake/raw/sex_census/DimenLookupSex8277.csv";
  let raw_year_path: &str = "data/minilake/raw/year_census/DimenLookupYear8277.csv";
  let raw_fato_path: &str = "data/minilake/raw/fato_census/Data8277.csv";
  /* ======================================
  Path for stage 
  ====================================== 
  */

  let stage_age_path: &str = "data/minilake/stage/age_census/";
  let stage_area_path: &str = "data/minilake/stage/area_census/";
  let stage_ethnic_path: &str = "data/minilake/stage/ethnic_census/";
  let stage_sex_path: &str = "data/minilake/stage/sex_census/";
  let stage_year_path: &str = "data/minilake/stage/year_census/";
  let stage_fato_path: &str = "data/minilake/stage/fato_census/";

  /* ======================================
  Path for stage 
  ====================================== 
  */
  let final_obt_path: &str = "data/minilake/curated/final_obt/";
  let aggregated_data_by_sex_path: &str = "data/minilake/curated/aggregated_tables/";

  // /* ======================================
  // Dataframe Creation Stage 
  // ====================================== 
  // */
  
  info!("Starting creating Context and read data");

  let fato_census_df = ctx.read_csv(raw_fato_path,  
                                  CsvReadOptions::new()).await?;
  let dim_age_census_df = ctx.read_csv( raw_age_path, CsvReadOptions::new()).await?;
  // Another form to create a dataframe
  ctx.register_csv("dim_area", raw_area_path, CsvReadOptions::new()).await?;
  // Create a plan to run a SQL query
  let dim_area_census_df = ctx.sql("SELECT * FROM dim_area").await?;

  let dim_ethnic_census_df = ctx.read_csv(raw_ethnic_path,CsvReadOptions::new()).await?;
  let dim_sex_census_df = ctx.read_csv(raw_sex_path,CsvReadOptions::new()).await?;
  let dim_year_census_df = ctx.read_csv(raw_year_path,CsvReadOptions::new()).await?;
  // /* ======================================
  // save Stage 
  // ====================================== 
  // */
  info!("Starting save Stage");
  // print first 5 rows of dataframe and your schema

  // We can process by loop every function
  let dataframes = vec![&fato_census_df, 
                                              &dim_age_census_df,
                                              &dim_area_census_df,
                                              &dim_ethnic_census_df,
                                              &dim_sex_census_df,
                                              &dim_year_census_df];

  for dataframe in dataframes.iter() {
    print_n_lines_of_dataframe(&dataframe, 5).await?;
    print_schema_of_dataframe(&dataframe).await?;
  }


  /* ======================================
  Transformation Stage - Schema Definition 
  ====================================== 
  */

  let dim_age_census_df = dim_age_census_df.select(vec![col("Code").alias("age_code"),
                                                                                   col("Description").alias("age_description")])?;
  dim_age_census_df.show_limit(5).await?;

  let dim_area_census_df = dim_area_census_df
                                             .select(vec![col("Code").alias("area_code"),
                                             col("Description").alias("area_description")])?;
  dim_area_census_df.show_limit(5).await?;

  let dim_ethnic_census_df = dim_ethnic_census_df
                                             .select(vec![col("Code").alias("ethnic_code"), 
                                             col("Description").alias("ethnic_description")])?;
  dim_ethnic_census_df.show_limit(5).await?;
  let dim_sex_census_df = dim_sex_census_df
                                             .select(vec![col("Code").alias("sex_code"), 
                                             col("Description").alias("sex_description")])?;
  dim_sex_census_df.show_limit(5).await?;
  let dim_year_census_df = dim_year_census_df
                                             .select(vec![col("Code").alias("year_code"), 
                                             col("Description").alias("year_description")])?;
  dim_year_census_df.show_limit(5).await?;

  let fato_census_df = fato_census_df
                                             .select(vec![
                                              col("Year").alias("year"),
                                              col("Age").alias("age"),
                                              col("Ethnic").alias("ethnic"),
                                              col("Sex").alias("sex") ,
                                              col("Area").alias("area"),
                                              col("count").alias("total_count")
                                             ])?;
  
  // We can see the ..C values in Count column
  fato_census_df.show_limit(20).await?;
  print_schema_of_dataframe(&fato_census_df).await?;
  // Create a function to make trnasformation
  let transform_count_data = when(col("total_count").eq(lit("..C")), lit(0_u32)).otherwise(col("total_count"))?;
  //Cast column datatype
  let fato_census_df = fato_census_df.with_column("total_count",cast(transform_count_data, Int64))?;
  fato_census_df.show_limit(25).await?;

  // let fato_census_df = fato_census_df.with_column("area",cast(
  //   col("area"),
  //   Utf8))?;
  fato_census_df.filter(col("area").eq(lit("CMB07601")))?.show_limit(5).await?;
  
  // Save data on silver layer
  info!("Saving Staged files");
  save_data_as_parquet(&dim_age_census_df, stage_age_path).await?;
  save_data_as_parquet(&dim_area_census_df, stage_area_path).await?;
  save_data_as_parquet(&dim_ethnic_census_df, stage_ethnic_path).await?;
  save_data_as_parquet(&dim_sex_census_df, stage_sex_path).await?;
  save_data_as_parquet(&dim_year_census_df, stage_year_path).await?;
  save_data_as_parquet(&fato_census_df, stage_fato_path).await?;


  /* ======================================
  Final Stage - One Big Table Creation
  ====================================== 
  */
  // Make a Join of all data
  let final_obt_df = fato_census_df.join(dim_age_census_df, 
                            JoinType::Inner, &["age_code"], 
                            &["age"], 
                             None)?;
  final_obt_df.show_limit(5).await?;
  // Create another aggregate tables and save in gold layer



  Ok(())
  }